﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.IO.Pipes;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace EasierLF2RecordingConverterConsole
{
    class Program
    {
        private static Mutex mutex_ = new Mutex(true, "EasierLF2RecordingConverterConsole");
        static BackgroundWorker Worker = new BackgroundWorker() { WorkerSupportsCancellation = true, WorkerReportsProgress = true };

        static void Main(string[] args)
        {
            if (mutex_.WaitOne(TimeSpan.Zero, true))
            {
                mutex_.ReleaseMutex();
                Worker.RunWorkerCompleted += BackgroundWorker_RunWorkerCompleted;
                Worker.RunWorkerAsync();
            }
            else
            {
                // debug
                var stream = new NamedPipeClientStream(".", "Easier LF2 Recording Converter - Debug Console", PipeDirection.Out);
                stream.Connect(100);
                byte[] bytes = System.Text.Encoding.UTF8.GetBytes("Hello Console!");
                stream.Write(bytes, 0, bytes.Length);
                stream.Close();
                return;
            }


            Console.Title = "Easier LF2 Recording Converter - Debug Console";
            Console.WriteLine("This is a console to debug the recorder library.");

            while (true) Console.ReadLine();
        }

        private static void BackgroundWorker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            List<PipelineServer> servers = new List<PipelineServer>();
            while (!Worker.CancellationPending)
            {
                var stream = new NamedPipeServerStream("Easier LF2 Recording Converter - Debug Console", PipeDirection.In, 100);
                stream.WaitForConnection();

                PipelineServer server = new PipelineServer(stream);
                server.ProgressChanged += Server_ProgressChanged;
                servers.Add(server);
                server.RunWorkerAsync();
            }

            foreach (PipelineServer server in servers)
            {
                if (server.IsBusy)
                    server.CancelAsync();
            }
        }

        private static void Server_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            if (e.UserState is string)
                Console.WriteLine((string)e.UserState);
        }
    }
}
