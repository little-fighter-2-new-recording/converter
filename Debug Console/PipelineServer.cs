﻿using System;
using System.ComponentModel;
using System.IO.Pipes;
using System.Text;

namespace EasierLF2RecordingConverterConsole
{
    class PipelineServer : BackgroundWorker
    {
        private NamedPipeServerStream Stream;

        public PipelineServer(NamedPipeServerStream stream)
        {
            this.WorkerReportsProgress = true;
            this.WorkerSupportsCancellation = true;

            this.Stream = stream;
            // this.Stream.ReadTimeout = 1000;
        }

        protected override void OnDoWork(DoWorkEventArgs e)
        {
            while (!CancellationPending && Stream.IsConnected)
            {
                try
                {
                    StringBuilder builder = new StringBuilder();
                    byte[] buffer = new byte[1024];
                    int readLen;
                    while ((readLen = this.Stream.Read(buffer, 0, buffer.Length)) > 0)
                    {
                        builder.Append(Encoding.UTF8.GetString(buffer, 0, readLen));
                        if (readLen < buffer.Length)
                            break;
                    }

                    if (builder.Length > 0)
                        ReportProgress(0, builder.ToString());

                }
                catch (TimeoutException ex)
                {
                    continue;
                }
                catch (Exception ex)
                {
                    ReportProgress(0, ex.Message);
                    break;
                }
            }
        }
    }
}
