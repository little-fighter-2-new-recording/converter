﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace EasierLF2RecordingConverter.Objects.Wrapper
{
    [StructLayout(LayoutKind.Sequential)]
    public struct sFrame
    {
        public char exists;
        public int pic;
        public int state;
        public int wait;
        public int next;
        public int dvx;
        public int dvy;
        public int dvz;
        public int unkwn1;
        public int hit_a;
        public int hit_d;
        public int hit_j;
        public int hit_Fa;
        public int hit_Ua;
        public int hit_Da;
        public int hit_Fj;
        public int hit_Uj;
        public int hit_Dj;
        public int hit_ja;
        public int mp;
        public int centerx;
        public int centery;
        public sOpoint opoint;
        public int unkwn2;
        public int unkwn3;
        public sBpoint bpoint;
        public sCpoint cpoint;
        public int unkwn4;
        public int unkwn5;
        public int unkwn6;
        public sWpoint wpoint;
        public int unkwn7;
        public int unkwn8;
        public int unkwn9;
        public int unkwn10;
        public int unkwn11;
        public int unkwn12;
        public int unkwn13;
        public int unkwn14;
        public int unkwn15;
        public int unkwn16;
        public int unkwn17;
        public int itr_count;
        public int bdy_count;
        //vv these are pointers to arrays
        //public sItr* itrs;
        public IntPtr itrs;

        //public sBdy* bdys;
        public IntPtr bdys;
        //vv these values form a rectangle that holds all itrs/bdys within it
        public int itr_x;
        public int itr_y;
        public int itr_w;
        public int itr_h;
        public int bdy_x;
        public int bdy_y;
        public int bdy_w;
        public int bdy_h;
        //----------------------------------------
        public int unkwn18;

        [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 20)]
        public string fname;

        public IntPtr sound; // maximum sound path is unknown actually

        public int unkwn19;
    }

    [StructLayout(LayoutKind.Sequential)]
    public struct sObject
    {
        public int move_counter;
        public int run_counter;
        public int blink;
        public int unkwn1;
        public int x;
        public int y;
        public int z;

        [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 12)]
        public string unkwn2;

        public double x_acceleration;
        public double y_acceleration;
        public double z_acceleration;
        public double x_velocity;
        public double y_velocity;
        public double z_velocity;
        public double x_real;
        public double y_real;
        public double z_real;
        public int frame1;
        public int frame2;
        public int frame3;
        public int frame4;
        public char facing;

        [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 7)]
        public string unkwn3;

        public int wait_counter;
        public int ccatcher;
        public int ctimer;
        public int unkwn3a;
        public int weapon_type;
        public int weapon_held;
        public int weapon_holder;
        public int unkwn4;

        [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 8)]
        public string unkwn5;
        public int fall;
        public int shake;
        public int bdefend;

        [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 10)]
        public string unkwn6;

        public char holding_up;
        public char holding_down;
        public char holding_left;
        public char holding_right;
        public char holding_a;
        public char holding_j;
        public char holding_d;
        public char up;
        public char down;
        public char left;
        public char right;
        public char A;
        public char J;
        public char D;
        public char DrA;
        public char DlA;
        public char DuA;
        public char DdA;
        public char DrJ;
        public char DlJ;
        public char DuJ;
        public char DdJ;
        public char DJA;

        [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 15)]
        public string unkwn7;
        public int arest;

        [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 400)]
        public string vrests;
        public int attacked_object_num;
        // skip unkwn8 for backwards compatibility reasons

        [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 112)]
        public string unkwn9;

        public int clone;
        public int weapon_thrower;
        public int hp;
        public int dark_hp;
        public int max_hp;
        public int mp;
        public int reserve;
        public int unkwn10;
        public int unkwn11;
        public int pic_gain;
        public int bottle_hp;
        public int throwinjury;

        [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 20)]
        public string unkwn12;

        public int firzen_counter;
        public int unkwn13;
        public int armour_multiplier;
        public int unkwn14;
        public int total_attack;
        public int hp_lost;
        public int mp_usage;
        public int unkwn15; // "owner" according to xsoameix
        public int kills;
        public int weapon_picks;
        public int enemy;
        public int team;

        // sDataFile *data;
        public IntPtr data;
    }

    [StructLayout(LayoutKind.Sequential)]
    public struct sFileManager
    {
        // sDataFile *datas[500]
        public IntPtr datas;

        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 60)]
        public sStage[] stages;

        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 50)]
        public sBackground[] backgrounds;
    }

    [StructLayout(LayoutKind.Sequential)]
    public struct sGame
    {
        public int state; // 0x4

        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 400)]
        public char[] exists;

        // sObject *objects[400]
        // [MarshalAs(UnmanagedType.ByValArray, SizeConst = 400)]
        public IntPtr objects;

        // sFileManager *files
        public IntPtr files;

        // old debug:
        //[MarshalAs(UnmanagedType.ByValArray, SizeConst = 400)]
        //public sObject[] objects; // 0x7d4
    }
}
