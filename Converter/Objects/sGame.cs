﻿using System;
using System.Runtime.InteropServices;

namespace EasierLF2RecordingConverter.Objects
{
    [StructLayout(LayoutKind.Sequential)]
    public struct sOpoint {
        public int kind;
        public int x;
        public int y;
        public int action;
        public int dvx;
        public int dvy;
        public int oid;
        public int facing;
    }

    [StructLayout(LayoutKind.Sequential)]
    public struct sBpoint {
        public int x;
        public int y;
    }

    [StructLayout(LayoutKind.Sequential)]
    public struct sCpoint
    {
        public int kind;
        public int x;
        public int y;
        public int injury; //if its kind 2 this is fronthurtact
        public int cover; // if its kind 2 this is backhurtact
        public int vaction;
        public int aaction;
        public int jaction;
        public int daction;
        public int throwvx;
        public int throwvy;
        public int hurtable;
        public int decrease;
        public int dircontrol;
        public int taction;
        public int throwinjury;
        public int throwvz;
    }

    [StructLayout(LayoutKind.Sequential)]
    public struct sWpoint
    {
        public int kind;
        public int x;
        public int y;
        public int weaponact;
        public int attacking;
        public int cover;
        public int dvx;
        public int dvy;
        public int dvz;
    }

    [StructLayout(LayoutKind.Sequential)]
    public struct sItr {
        public int kind;
        public int x;
        public int y;
        public int w;
        public int h;
        public int dvx;
        public int dvy;
        public int fall;
        public int arest;
        public int vrest;
        public int unkwn1; // named "respond" by xsoameix
        public int effect;
        public int catchingact1;
        public int catchingact2;
        public int caughtact1;
        public int caughtact2;
        public int bdefend;
        public int injury;
        public int zwidth;
        public int unkwn2;
    }

    [StructLayout(LayoutKind.Sequential)]
    public struct sBdy {
        public int kind;
        public int x;
        public int y;
        public int w;
        public int h;
        public int unkwn1;
        public int unkwn2;
        public int unkwn3;
        public int unkwn4;
        public int unkwn5;
    }

    [StructLayout(LayoutKind.Sequential)]
    public struct sDataFile
    {
        public int walking_frame_rate;
        public int unkwn1;
        public double walking_speed;
        public double walking_speedz;
        public int running_frame_rate;
        public double running_speed;
        public double running_speedz;
        public double heavy_walking_speed;
        public double heavy_walking_speedz;
        public double heavy_running_speed;
        public double heavy_running_speedz;
        public double jump_height;
        public double jump_distance;
        public double jump_distancez;
        public double dash_height;
        public double dash_distance;
        public double dash_distancez;
        public double rowing_height;
        public double rowing_distance;
        public int weapon_hp;
        public int weapon_drop_hurt;

        [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 1024)]
        public string unkwn2;
        public int pic_count;

        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 40 * 10)]
        public string[] pic_bmps;

        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 10)]
        public int[] pic_index;

        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 10)]
        public int[] pic_width;

        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 10)]
        public int[] pic_height;

        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 10)]
        public int[] pic_row;

        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 10)]
        public int[] pic_col;

        public int id;
        public int type;
        public int unkwn3;

        [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 40)]
        public string small_bmp; //I believe at least some of this has to do with small image
        public int unkwn4;

        [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 40)]
        public char face_bmp; //I believe at least some of this has to do with small image

        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 20)]
        public int[] unkwn5;

        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 400)]
        public Wrapper.sFrame[] frames;

        [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 12)]
        public string name; //not actually certain that the length is 12, seems like voodoo magic
    }

    [StructLayout(LayoutKind.Sequential)]
    public struct sSpawn
    {
        [MarshalAs(UnmanagedType.AsAny, SizeConst = 43)]
        public int[] unkwn1;//seems to have something to do with bosses, is changed during game so I believe it keeps track of whether or not soldiers should respawn

        public int id;
        public int x;
        public int hp;
        public int times;
        public int reserve;
        public int join;
        public int join_reserve;
        public int act;
        public int unkwn2;
        public double ratio;
        public int role; // 0 = normal, 1 = soldier, 2 = boss
        public int unkwn3;
    }

    [StructLayout(LayoutKind.Sequential)]
    public struct sPhase
    {
        public int bound;

        [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 52)]
        public string music;

        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 60)]
        sSpawn[] spawns;
        public int when_clear_goto_phase;
    }

    [StructLayout(LayoutKind.Sequential)]
    public struct sStage
    {
        public int phase_count;

        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 100)]
        public sPhase[] phases;
    }

    [StructLayout(LayoutKind.Sequential)]
    public struct sBackground
    {
        public int bg_width; //0x0
        public int bg_zwidth1; //0x4
        public int bg_zwidth2; // 0x8
        public int perspective1; //0xC
        public int perspective2; //0x10
        public int shadow1; //0x14
        public int shadow2; //0x18
        public int layer_count; //0x1c
        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 30 * 30)]
        public string[] layer_bmps; //0x20

        [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 30)]
        public string shadow_bmp; //0x3a4

        [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 30)]
        public string name; //0x3c2

        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 30)]
        public int[] transparency; //0x3e0

        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 30)]
        public int[] layer_width; // 0x458

        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 30)]
        public int[] layer_x; // 0x4d0

        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 30)]
        public int[] layer_y; // 0x548

        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 30)]
        public int[] layer_height; // 0x5c0

        [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 856)]
        public string unkwn1;
    }
}