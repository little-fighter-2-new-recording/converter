﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace EasierLF2RecordingConverter.Objects
{
    [StructLayout(LayoutKind.Sequential)]
    public struct sGameTest
    {
        public int state; // 0x4

        [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 400)]
        public string exists;
    }

    [StructLayout(LayoutKind.Sequential)]
    public struct SubStruct
    {
        public int id;
    }

    [StructLayout(LayoutKind.Sequential)]
    public struct TestStruct
    {
        public int id; // 0x4

        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 20)]
        public char[] charArray; // ye NO string!


        //[MarshalAs(UnmanagedType.LPArray, ArraySubType = UnmanagedType.LPStruct, SizeConst = 10)]
        public IntPtr subStructs;
    }
}
