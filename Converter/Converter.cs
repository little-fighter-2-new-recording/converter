﻿using System;
using System.IO.Pipes;
using System.Runtime.InteropServices;

namespace EasierLF2RecordingConverter
{
    public static class Converter
    {
        private static NamedPipeClientStream stream;

        public static void InitDll()
        {
            ConnectToDebugConsole();
            SendMessageToConsole("Started");
        }

        public static void AddSnapshot(IntPtr gamePtr)
        {
            SendMessageToConsole("Ptr To Struct");
            SendMessageToConsole("-------------");
            SendMessageToConsole("Ptr:  " + gamePtr);
            // Objects.Wrapper.sGame ver = Marshal.PtrToStructure<Objects.Wrapper.sGame>(test);
        }

        private static T[] GetStructures<T>(IntPtr pointer, int numbers)
        {
            if (pointer.ToInt64() <= 0)
                return new T[0];

            T[] elements = new T[numbers];
            int sizeOfT = Marshal.SizeOf(typeof(T));
            for (int i = 0; i < numbers; i++)
            {
                try
                {
                    elements[i] = Marshal.PtrToStructure<T>(pointer + sizeOfT * i);
                }
                catch (Exception ex)
                {
                    SendMessageToConsole("ERROR: " + ex.Message);
                }
            }

            return elements;
        }

        public static void WriteOnDebugConsole(string text)
        {
            SendMessageToConsole(text);
        }


        internal static void SendMessageToConsole(string text)
        {
            ConnectToDebugConsole();

            if (stream.IsConnected)
            {
                byte[] bytes = System.Text.Encoding.UTF8.GetBytes(text);
                stream.Write(bytes, 0, bytes.Length);
                stream.Flush();
            }
        }

        internal static void ConnectToDebugConsole()
        {
            if (stream == null || !stream.IsConnected)
            {
                stream = new NamedPipeClientStream(".", "Easier LF2 Recording Converter - Debug Console", PipeDirection.Out);
                try
                {
                    stream.Connect(200);
                }
                catch (Exception ex)
                { }
            }
        }
    }
}
