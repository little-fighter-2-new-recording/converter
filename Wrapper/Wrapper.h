#pragma once

#include <stdio.h>
#include <string.h>

#include "../Injected DLL/sgame.h"

using namespace System;
using namespace System::Runtime::InteropServices;

#ifndef _WRAPPER_CONVERTER_
#define _WRAPPER_CONVERTER_
//typedef struct {
//	int state; // 0x4
//	char exists[400]; // 0x194
//} sGameTest;


typedef struct {
	int id;
} subStruct;

typedef struct {
	int id;
	char chars[20];
	subStruct *subStructs[2];
} testStruct;

namespace EasierLF2RecordingConverterWrapper
{
	public ref class ConvertWrapper
	{
	public:void AddSnapshot(sGame *game) {
		EasierLF2RecordingConverter::Converter::AddSnapshot((IntPtr)game);
	}

	public:void InitDLL() {
		EasierLF2RecordingConverter::Converter::InitDll();
	}
	};
}
#endif