#include "Wrapper.h"

using namespace std;

__declspec(dllexport) void addSnapshot(sGame *value)
{
	EasierLF2RecordingConverterWrapper::ConvertWrapper work;
	work.AddSnapshot(value);
}

__declspec(dllexport) void initConverter() {
	EasierLF2RecordingConverterWrapper::ConvertWrapper work;
	work.InitDLL();
}