/*  DirectDraw Wrapper
 *  version 1.0, August 6th, 2010
 *
 *  Copyright (C) 2010 Jari Komppa
 *
 *  This software is provided 'as-is', without any express or implied
 *  warranty.  In no event will the authors be held liable for any damages
 *  arising from the use of this software.
 *
 *  Permission is granted to anyone to use this software for any purpose,
 *  including commercial applications, and to alter it and redistribute it
 *  freely, subject to the following restrictions:
 *
 *  1. The origin of this software must not be misrepresented; you must not
 *     claim that you wrote the original software. If you use this software
 *     in a product, an acknowledgment in the product documentation would be
 *     appreciated but is not required.
 *  2. Altered source versions must be plainly marked as such, and must not be
 *     misrepresented as being the original software.
 *  3. This notice may not be removed or altered from any source distribution.
 *
 * Jari Komppa http://iki.fi/sol/
 *
 *************************************
 *
 * Based on a zlib/libpng licensed source code found on the net,
 * http://www.mikoweb.eu/index.php?node=28
 * re-worked so much that there's fairly little left of the original.
 *
 *************************************
 * Modified by Doix - not the original wrapper
 * More modifications made by Someone else
 */

#include "stdafx.h"
#include <varargs.h>
#include "detours.h"
#include <sys/stat.h>
#include <sstream>
#include <windows.h>

#include "sgame.h"

_declspec(dllexport) void addSnapshot(sGame *value);
_declspec(dllexport) void initConverter();

// global variables
#pragma data_seg (".ddraw_shared")
HINSTANCE           gl_hOriginalDll;
HINSTANCE           gl_hThisInstance;
#pragma data_seg ()

int mode;
int difficulty;
int elapsed_time;
int background = -1;
int bg_width;
int bg_zwidth1;
int bg_zwidth2;
int stage_bound;
int current_phase;
int current_phase_count;
int current_stage;
char stage_clear;
int game_is_over;

sGame* game = (sGame*)0x458b00;

#ifdef DEBUG_VERSION
#include <map>

std::map<int, FILETIME> ModuleTimes;
#endif

void startup();
void setEnvVariables();

int(__stdcall* AI_o)(int target_num, int object_num, int x, int y, int z, int a, int b);
int(__stdcall* AIa_o)(int object_num, int unkwn1);

int random(int max) {//random function that works with replays
	int result; // eax@2
	signed int v3; // eax@3

	DWORD& dword_450C34 = *((DWORD*)0x450C34);
	DWORD& dword_450BCC = *((DWORD*)0x450BCC);

	if (max > 0) {
		dword_450C34 = (dword_450C34 + 1) % 1234;
		v3 = dword_450C34 + (unsigned __int8)*(((BYTE*)0x44FF90) + (dword_450BCC + 1) % 3000);
		dword_450BCC = (dword_450BCC + 1) % 3000;
		result = v3 % max;
	}
	else {
		result = 0;
	}

	return result;
}


void clr() {
	HANDLE hConsole = GetStdHandle(STD_OUTPUT_HANDLE);

	if (hConsole == INVALID_HANDLE_VALUE) return;

	COORD coordScreen = { 0,0 };
	DWORD cCharsWritten;
	CONSOLE_SCREEN_BUFFER_INFO csbi;
	DWORD dwConSize;

	if (!GetConsoleScreenBufferInfo(hConsole, &csbi)) {
		return;
	}

	dwConSize = csbi.dwSize.X * csbi.dwSize.Y;

	if (!FillConsoleOutputCharacter(hConsole, ' ', dwConSize, coordScreen, &cCharsWritten)) {
		return;
	}

	if (!GetConsoleScreenBufferInfo(hConsole, &csbi)) {
		return;
	}

	if (!FillConsoleOutputAttribute(hConsole, csbi.wAttributes, dwConSize, coordScreen, &cCharsWritten)) {
		return;
	}

	SetConsoleCursorPosition(hConsole, coordScreen);
}

void printAddr(void* Addr) {
	printf("%p", Addr);
}

void print(bool p) {
	if (p) {
		printf("true");
	}
	else {
		printf("false");
	}
}

void print(char p) {
	printf("%d", p);
}
void print(short p) {
	printf("%d", p);
}
void print(int p) {
	printf("%d", p);
}
void print(long long p) {
	printf("%ld", p);
}

void print(unsigned char p) {
	printf("%u", p);
}
void print(unsigned short p) {
	printf("%u", p);
}
void print(unsigned int p) {
	printf("%u", p);
}
void print(unsigned long long p) {
	printf("%lu", p);
}

void print(float p) {
	printf("%f", p);
}
void print(double p) {
	printf("%Lf", p);
}

void print(const std::string& p) {
	printf("%s", p.c_str());
}

int __stdcall AI(int target_num, int object_num, int unkwn1, int unkwn2, int unkwn3, int unkwn4, int unkwn5)
{
	DWORD return_value;
	DWORD unkwn6;
	__asm
	{
		mov dword ptr ds : [unkwn6], ecx;
	}
	int id_int = game->objects[object_num]->data->id;
	printf("TEST: %d\n", id_int);

	__asm
	{
		mov ecx, dword ptr ds : [unkwn5];
		push ecx;
		mov ecx, dword ptr ds : [unkwn4];
		push ecx;
		mov ecx, dword ptr ds : [unkwn3];
		push ecx;
		mov ecx, dword ptr ds : [unkwn2];
		push ecx;
		mov ecx, dword ptr ds : [unkwn1];
		push ecx;
		mov ecx, dword ptr ds : [object_num];
		push ecx;
		mov ecx, dword ptr ds : [target_num];
		push ecx;
		mov ecx, dword ptr ds : [unkwn6];
		call AI_o;
		mov dword ptr ds : [return_value], eax;
	}

	return return_value;
}

void __stdcall AIa(int object_num, int unkwn1)
{
	DWORD unkwn2;
	_asm
	{
		mov dword ptr ds : [unkwn2], ecx;
	}
	int id_int = game->objects[object_num]->data->id;

	setEnvVariables();
	printf("ID INT: %d %d %d %d\n", id_int, mode, elapsed_time, game_is_over);

	_asm
	{
		mov ecx, dword ptr ds : [unkwn1];
		push ecx;
		mov ecx, dword ptr ds : [object_num];
		push ecx;
		mov ecx, dword ptr ds : [unkwn2];
		call AIa_o;
	}
}

void startup()
{
#ifdef DEBUG_VERSION
	AllocConsole();
	freopen("CONIN$", "rb", stdin);   // reopen stdin handle as console window input
	freopen("CONOUT$", "wb", stdout);  // reopen stout handle as console window output
	freopen("CONOUT$", "wb", stderr); // reopen stderr handle as console window output
#endif

	AI_o = (int(__stdcall*)(int, int, int, int, int, int, int))DetourFunction((PBYTE)0x00403a40, (PBYTE)AI);
	AIa_o = (int(__stdcall*)(int, int))DetourFunction((PBYTE)0x004094b0, (PBYTE)AIa);
}

void setEnvVariables() {
	elapsed_time = *(int*)0x450b8c;
	difficulty = *(int*)0x450c30;
	mode = *(int*)0x451160;
	if (background != *(int*)0x44d024) {
		background = *(int*)0x44d024;
		bg_width = game->files->backgrounds[background].bg_width;
		bg_zwidth1 = game->files->backgrounds[background].bg_zwidth1;
		bg_zwidth2 = game->files->backgrounds[background].bg_zwidth2;

		stage_bound = bg_width;
		stage_clear = false;
	}
	if (mode == 1) {//stage
		stage_bound = *(int*)0x450bb4;
		stage_clear = stage_bound == 0;
	}
	current_phase = *(int*)0x44fb6c;
	current_phase_count = *(int*)0x44f880;
	current_stage = *(int*)0x450b94;
	game_is_over = *(int*)0x450b80;
}

BOOL APIENTRY DllMain(HANDLE hModule, DWORD  ul_reason_for_call, LPVOID lpReserved)
{
	LPVOID lpDummy = lpReserved;
	lpDummy = NULL;

	switch (ul_reason_for_call)
	{
	case DLL_PROCESS_ATTACH:
		startup();
		InitInstance(hModule); break;
	case DLL_PROCESS_DETACH:
		ExitInstance(); break;
	case DLL_THREAD_ATTACH:
		break;
	case DLL_THREAD_DETACH:
		break;

	}
	return(true);
}


HRESULT WINAPI DirectDrawCreate(GUID FAR* lpGUID, LPDIRECTDRAW FAR* lplpDD, IUnknown FAR* pUnkOuter)
{

	if (!gl_hOriginalDll) {
		LoadAIOrOriginalDll(); // looking for the "right ddraw.dll"
	}

	// Hooking DDRAW interface from Original Library IDirectDraw *pDD;

	typedef HRESULT(WINAPI* DirectDrawCreate_Type)(GUID FAR*, LPVOID*, IUnknown FAR*);

	DirectDrawCreate_Type DirectDrawCreate_fn = (DirectDrawCreate_Type)GetProcAddress(gl_hOriginalDll, _T("DirectDrawCreate"));

	if (!DirectDrawCreate_fn)
	{
		::ExitProcess(0);
	}

	LPDIRECTDRAW7 FAR dummy;
	HRESULT h = DirectDrawCreate_fn(lpGUID, (LPVOID*)&dummy, pUnkOuter);
	*lplpDD = (LPDIRECTDRAW) new myIDDraw7(dummy);

	addSnapshot(game);

	printf("Capture Frame\n");
	return (h);
}

void InitInstance(HANDLE hModule)
{

	// Initialisation
	gl_hOriginalDll = NULL;
	gl_hThisInstance = NULL;

	// Storing Instance handle into global var
	gl_hThisInstance = (HINSTANCE)hModule;
}

bool LoadAIDll() {
	char buffer[MAX_PATH];
	::GetCurrentDirectory(MAX_PATH, buffer);
	// Append dll name
	strcat(buffer, "\\ddraw_ai.dll");

	FILE *file;
	if (file = fopen(buffer, "r")) {
		fclose(file);

		printf("Load ai dll %s\n", buffer);

		if (!gl_hOriginalDll) gl_hOriginalDll = ::LoadLibrary(buffer);

		return true;
	}

	return false;
}

void LoadAIOrOriginalDll(void)
{
	if (LoadAIDll()) {
		return;
	}

	char buffer[MAX_PATH];
	// Getting path to system dir and to d3d9.dll
	::GetSystemDirectory(buffer, MAX_PATH);

	// Append dll name
	strcat(buffer, "\\ddraw.dll");

	printf("Load original dll %s\n", buffer);

	if (!gl_hOriginalDll) gl_hOriginalDll = ::LoadLibrary(buffer);

	// Debug
	if (!gl_hOriginalDll)
	{
		::ExitProcess(0); // exit the hard way
	}
}

void ExitInstance()
{
	if (gl_hOriginalDll)
	{
		::FreeLibrary(gl_hOriginalDll);
		gl_hOriginalDll = NULL;
	}
}